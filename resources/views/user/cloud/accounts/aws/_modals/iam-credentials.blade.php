<div class="modal fade" id="iam-credentials-{{ $cloud_account_user->short_id }}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">IAM Credentials</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-right font-weight-bold">Cloud Account</label>
                    <div class="col-md-9 col-form-label">
                        {{ $cloud_account->slug }}-{{ $cloud_account->short_id }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-right font-weight-bold">Cloud Account User</label>
                    <div class="col-md-9 col-form-label">
                        {{ $cloud_account_user->authUser->full_name }}
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-right font-weight-bold">Cloud Account Roles</label>
                    <div class="col-md-9 col-form-label">
                        @foreach($cloud_account_user->cloudAccountUserRoles as $cloud_account_user_role)
                            {{ $cloud_account_user_role->cloudAccountRole->api_name }}<br />
                        @endforeach
                    </div>
                </div>

                <hr />

                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-right font-weight-bold">AWS Console URL</label>
                    <div class="col-md-9 col-form-label">
                        @if(array_key_exists('Id', $cloud_account_user->cloudAccount->api_meta_data))
                            <a target="_blank" href="https://{{ $cloud_account_user->cloudAccount->api_meta_data['Id'] }}.signin.aws.amazon.com/console">https://{{ $cloud_account_user->cloudAccount->api_meta_data['Id'] }}.signin.aws.amazon.com/console</a>
                        @else
                            The AWS account has not been provisioned yet.
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-right font-weight-bold">Username</label>
                    <div class="col-md-9 col-form-label">
                        @if($cloud_account_user->username != null && $cloud_account_user->state == 'active')
                            <code>{{ $cloud_account_user->username }}</code>
                        @else
                            The AWS IAM user account has not been provisioned yet.
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label text-right font-weight-bold">Password</label>
                    <div class="col-md-9 col-form-label">
                        @if($cloud_account_user->password != null && $cloud_account_user->state == 'active')
                            <code>{{ decrypt($cloud_account_user->password) }}</code>
                        @else
                            No password is configured for this IAM account.
                        @endif
                    </div>
                </div>

                @if($cloud_account->git_meta_data)

                    <hr />

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-right font-weight-bold">GitOps URL</label>
                        <div class="col-md-9 col-form-label">
                            @if(array_key_exists('web_url', $cloud_account->git_meta_data))
                                <a target="_blank" href="{{ $cloud_account->git_meta_data['web_url'] }}">{{ $cloud_account->git_meta_data['web_url'] }}</a>
                            @else
                                The GitOps group has not been provisioned. Please contact the System Administrator for assistance.
                            @endif
                        </div>
                    </div>
    
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-right font-weight-bold">Username</label>
                        <div class="col-md-9 col-form-label">
                            @if($cloud_account_user->authUser->git_username != null && $cloud_account_user->authUser->flag_git_user_provisioned == true)
                                <code>{{ $cloud_account_user->authUser->git_username }}</code>
                            @else
                                Your GitOps user account has not been provisioned yet.
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label text-right font-weight-bold">Password</label>
                        <div class="col-md-9 col-form-label">
                            @if($cloud_account_user->authUser->git_username != null && $cloud_account_user->authUser->flag_git_user_provisioned == true)
                                <code>{{ decrypt($cloud_account_user->authUser->git_password) }}</code>
                            @else
                                Your GitOps user account has not been provisioned yet.
                            @endif
                        </div>
                    </div>

                @endif

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
