<?php

namespace Tests\Unit\app\Services\V1\Vendor\Gcp;

use Tests\Fakes\CloudDnsRecordSetServiceFake;

it('can check for an existing dns record', function(){
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $client = new CloudDnsRecordSetServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));
    $record_set = $client->checkForDnsRecordSet([
        'managed_zone' => 'testing-zone',
        'name' => 'testing' . config('tests.gcp.gcp_dns_suffix'),
        'type' => 'CNAME'
    ]);
    expect($record_set->object->rrdatas[0])->toBe('testing1.example.com.');
});

it('can check for a non-existing dns record set', function(){
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $client = new CloudDnsRecordSetServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));
    $record_set = $client->checkForDnsRecordSet([
        'managed_zone' => 'testing-zone',
        'name' => 'faketesting' . config('tests.gcp.gcp_dns_suffix'),
        'type' => 'CNAME'
    ]);
    expect($record_set->status->ok)->toBeFalse();
    expect($record_set->object->error->code)->toBe(404);
});

it('can create a recordset object properly', function(){
    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $client = new CloudDnsRecordSetServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));
    $response = $client->createDnsRecordSet([
        'managed_zone' => 'testing-zone',
        'name' => 'testingmail' . config('tests.gcp.gcp_dns_suffix'),
        'type' => 'CNAME',
        'ttl' => 300,
        'rrdatas' => ['mail.testingzone.example.com.']
    ]);
    expect($response->status->code)->toBe(200);
});

it('can delete a recordset object properly', function(){

    $gcp_cloud_provider = \App\Models\Cloud\CloudProvider::where('type', 'gcp')->firstOrFail();
    $client = new CloudDnsRecordSetServiceFake($gcp_cloud_provider->id, config('tests.gcp.project_id'));
    $response = $client->deleteDnsRecordSet([
        'managed_zone' => 'testing-zone',
        'name' => 'testingmail' . config('tests.gcp.gcp_dns_suffix'),
        'type' => 'CNAME',
    ]);
    expect($response->status->code)->toBe(200);
});
