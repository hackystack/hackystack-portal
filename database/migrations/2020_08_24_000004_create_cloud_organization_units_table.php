<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCloudOrganizationUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cloud_organization_units', function (Blueprint $table) {
            $table->uuid('id')->nullable()->index();
            $table->string('short_id', 8)->nullable()->index();
            $table->uuid('auth_tenant_id')->nullable()->index();
            $table->uuid('cloud_organization_unit_id_parent')->nullable()->index();
            $table->uuid('cloud_provider_id')->nullable()->index();
            $table->uuid('cloud_realm_id')->nullable()->index();
            $table->json('api_meta_data')->nullable();
            $table->json('git_meta_data')->nullable();
            $table->string('name');
            $table->string('slug')->nullable()->index();
            $table->boolean('flag_provisioned')->default(false)->nullable();
            $table->timestamp('provisioned_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->string('state', 55)->nullable();
            $table->unique(['cloud_provider_id', 'slug'], 'cloud_organization_units_provider_slug_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cloud_organization_units');
    }
}
