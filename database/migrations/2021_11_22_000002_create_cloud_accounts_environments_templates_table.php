<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCloudAccountsEnvironmentsTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cloud_accounts_environments_templates', function (Blueprint $table) {
            $table->uuid('id')->index();
            $table->string('short_id', 8)->index();
            $table->uuid('auth_tenant_id')->index();
            $table->uuid('cloud_provider_id')->index();
            $table->json('git_meta_data')->nullable();
            $table->string('git_name', 255)->nullable();
            $table->text('git_description')->nullable();
            $table->string('git_project_id', 20)->nullable();
            $table->string('name', 55)->nullable();
            $table->string('description', 255)->nullable();
            $table->tinyInteger('display_order')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->string('state', 55)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cloud_accounts_environments_templates');
    }
}
