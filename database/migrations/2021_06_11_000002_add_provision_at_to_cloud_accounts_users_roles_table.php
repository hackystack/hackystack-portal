<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProvisionAtToCloudAccountsUsersRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('provision_at', 'cloud_accounts_users_roles')) {
            return;
        }

        Schema::table('cloud_accounts_users_roles', function (Blueprint $table) {
            $table->timestamp('provision_at')->nullable()->after('flag_provisioned');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cloud_accounts_users_roles', function (Blueprint $table) {
            $table->dropColumn('provision_at');
        });
    }
}
