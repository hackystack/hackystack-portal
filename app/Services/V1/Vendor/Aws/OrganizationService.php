<?php

namespace App\Services\V1\Vendor\Aws;

use App\Services\V1\Vendor\Aws\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class OrganizationService extends BaseService
{

    protected $aws_api_client;

    public function __construct($cloud_provider_id)
    {
        // Call BaseService methods to establish API connection
        $this->setCloudProvider($cloud_provider_id);

        // Decrypt the provider credentials to be able to use the array keys
        $credentials = json_decode(decrypt($this->cloud_provider->api_credentials), true);

        // Initialize the API client
        $this->aws_api_client = new \Aws\Organizations\OrganizationsClient([
            'version' => 'latest',
            'region'  => 'us-east-1',
            'credentials' => [
                'key'    => $credentials['aws_access_key_id'],
                'secret' => $credentials['aws_access_key_secret'],
            ],
        ]);

    }

    /**
     *   Get an AWS Organization
     *
     *   @param  string $id     Account ID
     *
     *   @return array
     *      'Arn' => '<string>',
     *      'AvailablePolicyTypes' => [
     *          ...
     *      ],
     *      'FeatureSet' => 'ALL|CONSOLIDATED_BILLING',
     *      'Id' => '<string>',
     *      'MasterAccountArn' => '<string>',
     *      'MasterAccountEmail' => '<string>',
     *      'MasterAccountId' => '<string>',
     *      'Root' => [
     *          'Arn' => '<string>',
     *          'Id' => '<string>',
     *          'Name' => '<string>',
     *          'PolicyTypes' => [...]
     *      ],
     */
    public function get()
    {
        // Get organization details
        $record = $this->aws_api_client->describeOrganization([]);

        // Get root organization units
        $roots = $this->aws_api_client->listRoots([]);

        // Add roots to returned array
        $record['Organization']['Root'] = $roots['Roots'][0];

        return $record['Organization'];
    }

}
