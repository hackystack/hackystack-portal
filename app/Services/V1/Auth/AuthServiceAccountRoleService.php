<?php

namespace App\Services\V1\Auth;

use App\Services\BaseService;
use App\Models;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AuthServiceAccountRoleService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Auth\AuthServiceAccountRole::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_tenant_id          required|uuid|exists:auth_tenants,id
     *      auth_service_account_id required|uuid|exists:auth_service_accounts,id
     *      auth_role_id            required|uuid|exists:auth_roles,id
     *      expires_at              nullable|datetime
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Get Tenant relationship
        if(!empty($request_data['auth_tenant_id'])) {

            // Get relationship by ID to validate that it exists
            $tenant = Models\Auth\AuthTenant::query()
                ->where('id', $request_data['auth_tenant_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $tenant->id;
        }

        // Get Service Account relationship
        if(!empty($request_data['auth_service_account_id'])) {

            // Get relationship by ID to validate that it exists
            $service_account = Models\Auth\AuthServiceAccount::query()
                ->where('id', $request_data['auth_service_account_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_service_account_id = $service_account->id;
        }

        // Get Role relationship
        if(!empty($request_data['auth_role_id'])) {

            // Get relationship by ID to validate that it exists
            $role = Models\Auth\AuthRole::query()
                ->where('id', $request_data['auth_role_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_role_id = $role->id;
        }

        // Calculate expires at value
        if(!empty($request_data['expires_at'])) {

            $expires_at = \Carbon\Carbon::parse($request_data['expires_at']);

            if($request_data['expires_at'] == null) {
                $record->expires_at = null;
            } elseif($expires_at > now()) {
                $record->expires_at = $expires_at;
            } elseif($expires_at <= now()) {
                abort(400, 'The `expires_at` value for the group role cannot be in the past.');
            }

        }

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      expires_at             nullable|datetime
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {
        //
        // Update the existing record
        // --------------------------------------------------------------------
        //

        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Calculate expires at value
        if(!empty($request_data['expires_at'])) {

            // If request value is different than record existing value
            if($record->expires_at != $request_data['expires_at']) {

                $expires_at = \Carbon\Carbon::parse($request_data['expires_at']);

                if($request_data['expires_at'] == null) {
                    $record->expires_at = null;
                } elseif($expires_at > now()) {
                    $record->expires_at = $expires_at;
                } elseif($expires_at <= now()) {
                    abort(400, 'The `expires_at` value for the group role cannot be in the past.');
                }

            }
        }

        $record->save();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;

    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

}
