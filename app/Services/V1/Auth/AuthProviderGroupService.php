<?php

namespace App\Services\V1\Auth;

use App\Services\BaseService;
use App\Models;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AuthProviderGroupService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Auth\AuthProviderGroup::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_tenant_id         required|uuid|exists:auth_tenants,id
     *      auth_provider_id       required|uuid|exists:auth_providers,id
     *      auth_group_id          required|uuid|exists:auth_groups,id
     *      type                   required|in:default,meta
     *      meta_key               nullable|string|max:55
     *      meta_value             nullable|string|max:255
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Get Tenant relationship
        if(!empty($request_data['auth_tenant_id'])) {

            // Get relationship by ID to validate that it exists
            $tenant = Models\Auth\AuthTenant::query()
                ->where('id', $request_data['auth_tenant_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $tenant->id;
        }

        // Get Provider relationship
        if(!empty($request_data['auth_provider_id'])) {

            // Get relationship by ID to validate that it exists
            $provider = Models\Auth\AuthProvider::query()
                ->where('id', $request_data['auth_provider_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_provider_id = $provider->id;
        }

        // Get Group relationship
        if(!empty($request_data['auth_group_id'])) {

            // Get relationship by ID to validate that it exists
            $group = Models\Auth\AuthGroup::query()
                ->where('id', $request_data['auth_group_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_group_id = $group->id;
        }

        // Calculate type value
        if(!empty($request_data['type'])) {

            if($request_data['type'] == 'default') {
                $record->type = 'default';
                $record->meta_key = null;
                $record->meta_value = null;
            } elseif($request_data['type'] == 'meta') {
                $record->type = 'meta';
                $record->meta_key = $request_data['meta_key'];
                $record->meta_value = $request_data['meta_value'];
            } else {
                abort(400, 'The `type` value for the provider group is not a valid value. Allowed values are `default`, `meta`.');
            }

        }

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      type                    nullable|string
     *      meta_key                nullable|string
     *      meta_value              nullable|string
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {
        //
        // Update the existing record
        // --------------------------------------------------------------------
        //

        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // If `type` is set in request
        if(!empty($request_data['type'])) {

            if($request_data['type'] == 'default') {
                $record->type = ' default';
                $record->meta_key = null;
                $record->meta_value = null;
            } elseif($request_data['type'] == 'meta') {
                $record->type = 'meta';
                $record->meta_key = $request_data['meta_key'];
                $record->meta_value = $request_data['meta_value'];
            } else {
                abort(400, 'The `type` value for the provider group is not a valid value. Allowed values are `default`, `meta`.');
            }

        } else {

            // If record has a meta type
            if($record->type == 'meta') {

                // Check if meta_key has been set in request
                if(!empty($request_data['meta_key'])) {
                    // Check if meta key in request is different than database
                    if($request_data['meta_key'] != $record->meta_key) {
                        $record->meta_key = $request_data['meta_key'];
                    }
                }

                // Check if meta_value has been set in request
                if(!empty($request_data['meta_value'])) {
                    // Check if meta value in request is different than database
                    if($request_data['meta_value'] != $record->meta_value) {
                        $record->meta_value = $request_data['meta_value'];
                    }
                }

            }

        }

        $record->save();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;

    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

}
