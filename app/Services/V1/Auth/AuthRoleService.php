<?php

namespace App\Services\V1\Auth;

use App\Services\BaseService;
use App\Models;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class AuthRoleService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Auth\AuthRole::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_tenant_id          required|uuid|exists:auth_tenants,id
     *      name                    required|string|max:55
     *      slug                    nullable|string|max:55
     *      description             nullable|text
     *      flag_is_elevated        nullable|integer|between:0,1
     *      flag_is_group_member    nullable|integer|between:0,1
     *      permissions             required|array
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Get Tenant relationship
        if(!empty($request_data['auth_tenant_id'])) {

            // Get relationship by ID to validate that it exists
            $tenant = Models\Auth\AuthTenant::query()
                ->where('id', $request_data['auth_tenant_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $tenant->id;
        }

        // Text fields
        $record->name = Arr::get($request_data, 'name');
        $record->slug = Arr::get($request_data, 'slug');
        $record->description = Arr::get($request_data, 'description');
        $record->flag_is_elevated = Arr::get($request_data, 'flag_is_elevated');
        $record->flag_is_group_member = Arr::get($request_data, 'flag_is_group_member');
        $record->permissions = Arr::get($request_data, 'permissions', []); // TODO enforce array encoding or json conversion

        // TODO Add log entry for elevated roles

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      name                    nullable|string|max:55
     *      slug                    nullable|string|max:55
     *      description             nullable|text
     *      flag_is_elevated        nullable|integer|between:0,1
     *      flag_is_group_member    nullable|integer|between:0,1
     *      permissions             nullable|array
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {
        //
        // Update the existing record
        // --------------------------------------------------------------------
        //

        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Text fields
        $record->name = Arr::get($request_data, 'name', $record->name);
        $record->slug = Arr::get($request_data, 'slug', $record->slug);
        $record->description = Arr::get($request_data, 'description', $record->description);
        $record->flag_is_elevated = Arr::get($request_data, 'flag_is_elevated', $record->flag_is_elevated);
        $record->flag_is_group_member = Arr::get($request_data, 'flag_is_group_member', $record->flag_is_group_member);
        $record->permissions = Arr::get($request_data, 'permissions', $record->permissions); // TODO enforce array encoding or json conversion

        // TODO Add log entry for elevated role change

        $record->save();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;

    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

}
