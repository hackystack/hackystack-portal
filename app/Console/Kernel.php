<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // Cloud Accounts - Check for cloud accounts that have completed the
        // provisioning process and provision child relationships
        $schedule->command('cloud-account:create-status-refresh')
            ->everyMinute()
            ->appendOutputTo(storage_path('logs/cron.log'));

        // Cloud Account Users - Check for cloud account users that have a
        // provision_at scheduled time to be provisioned at.
        $schedule->command('cloud-account-user:scheduled-provisioning')
            ->everyMinute()
            ->appendOutputTo(storage_path('logs/cron.log'));

        // Cloud Account Users Roles - Check for cloud account users that have a 
        // provision_at scheduled time to be provisioned at.
        $schedule->command('cloud-account-user-role:scheduled-provisioning')
            ->everyMinute()
            ->appendOutputTo(storage_path('logs/cron.log'));
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        $this->load(__DIR__.'/Commands/Auth');
        $this->load(__DIR__.'/Commands/Cloud');
        $this->load(__DIR__.'/Commands/Developer');

        require base_path('routes/console.php');
    }
}
