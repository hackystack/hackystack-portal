<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class CloudAccountGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account:get
                            {short_id? : The short ID of the cloud account.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a Cloud Account by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of accounts using `cloud-account:list`');
            $this->line('You can lookup by short ID using `cloud-account:get a1b2c3d4`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_account = Models\Cloud\CloudAccount::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_account == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Cloud Account Values
        $this->comment('');
        $this->comment('Cloud Account');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $cloud_account->id
                ],
                [
                    'short_id',
                    $cloud_account->short_id
                ],
                [
                    'auth_tenant_id',
                    '['.$cloud_account->authTenant->short_id.'] '.$cloud_account->authTenant->slug
                ],
                [
                    'cloud_organization_unit_id',
                    '['.$cloud_account->cloudOrganizationUnit->short_id.'] '.$cloud_account->cloudOrganizationUnit->slug
                ],
                [
                    'cloud_provider_id',
                    '['.$cloud_account->cloudProvider->short_id.'] '.$cloud_account->cloudProvider->slug
                ],
                [
                    'cloud_realm_id',
                    '['.$cloud_account->cloudRealm->short_id.'] '.$cloud_account->cloudRealm->slug
                ],
                [
                    'name',
                    $cloud_account->name
                ],
                [
                    'slug',
                    $cloud_account->slug
                ],
                [
                    'created_at',
                    $cloud_account->created_at->toIso8601String()
                ],
                [
                    'provisioned_at',
                    $cloud_account->provisioned_at ? $cloud_account->provisioned_at->toIso8601String() : 'not provisioned'
                ],
                [
                    'state',
                    $cloud_account->state
                ]
            ]
        );

        $api_meta_data_values = [];
        foreach($cloud_account->api_meta_data as $meta_data_key => $meta_data_value) {
            $api_meta_data_values[] = [
                $meta_data_key,
                is_array($meta_data_value) ? json_encode($meta_data_value) : $meta_data_value
            ];
        }

        $this->table(
            ['API Meta Data Column', 'API Value'],
            $api_meta_data_values
        );

        //
        // Cloud Account - Child Relationship - Cloud Account Roles
        // --------------------------------------------------------------------
        // Loop through cloud account roles in Eloquent model and add
        // calculated values to array. The dot notation for pivot relationships
        // doesn't work with a get([]) so this is the best way to handle this.
        //

        // Loop through records and add values to array
        $cloud_account_role_rows = [];
        foreach($cloud_account->cloudAccountRoles as $role) {
            $cloud_account_role_rows[] = [
                'short_id' => $role->short_id,
                'api_name' => $role->api_name,
                'created_at' => $role->created_at->toIso8601String(),
            ];
        }

        $this->comment('');
        $this->comment('Cloud Account Roles');

        // If rows exists, render a table or return a no results found message
        if(count($cloud_account_role_rows) > 0) {
            $this->table(
                ['Role Short ID', 'API Name', 'Created at'],
                $cloud_account_role_rows
            );
        } else {
            $this->error('No cloud account roles have been created.');
        }

        //
        // Cloud Account - Child Relationship - Cloud Account Groups
        // --------------------------------------------------------------------
        // Loop through cloud account groups in Eloquent model and add
        // calculated values to array. The dot notation for pivot relationships
        // doesn't work with a get([]) so this is the best way to handle this.
        //

        // Loop through cloud account groups and add values to array
        $cloud_account_group_rows = [];
        foreach($cloud_account->cloudAccountGroups as $group) {
            $cloud_account_group_rows[] = [
                'short_id' => $group->short_id,
                'auth_group_short_id' => $group->authGroup->short_id,
                'name' => $group->authGroup->name,
                'slug' => $group->authGroup->slug,
            ];
        }

        $this->comment('');
        $this->comment('Cloud Account Groups');

        // If rows exists, render a table or return a no results found message
        if(count($cloud_account_group_rows) > 0) {
            $this->table(
                ['Cloud Account Group Short ID', 'Auth Group Short ID', 'Group Name', 'Group Slug'],
                $cloud_account_group_rows
            );
        } else {
            $this->error('No authentication groups groups have been attached as a cloud account group.');
        }

        //
        // Cloud Account - Child Relationship - Cloud Account Group Roles
        // --------------------------------------------------------------------
        // Loop through cloud account group roles in Eloquent model and add
        // calculated values to array. The dot notation for pivot relationships
        // doesn't work with a get([]) so this is the best way to handle this.
        //

        // Loop through records and add values to array
        $cloud_account_group_role_rows = [];
        foreach($cloud_account->cloudAccountGroupsRoles()->orderBy('cloud_account_role_id')->get() as $group_role) {
            $cloud_account_group_role_rows[] = [
                'short_id' => $group_role->short_id,
                'cloud_account_group' => '['.$group_role->cloudAccountGroup->short_id.'] '.$group_role->cloudAccountGroup->slug,
                'cloud_account_role' => '['.$group_role->cloudAccountRole->short_id.'] '.$group_role->cloudAccountRole->api_name,
                'created_at' => $group_role->created_at->toIso8601String(),
                'expires_at' => $group_role->expires_at ? $group_role->expires_at->toIso8601String() : 'never'
            ];
        }

        $this->comment('');
        $this->comment('Cloud Account Group Roles');

        // If rows exists, render a table or return a no results found message
        if(count($cloud_account_group_role_rows) > 0) {
            $this->table(
                ['Group Role Short ID', 'Cloud Account Group', 'Cloud Account Role', 'Created at', 'Expires at'],
                $cloud_account_group_role_rows
            );
        } else {
            $this->error('No cloud account roles have been created.');
        }

        //
        // Cloud Account - Child Relationship - Cloud Account Users
        // --------------------------------------------------------------------
        // Loop through cloud account users in Eloquent model and add
        // calculated values to array. The dot notation for pivot relationships
        // doesn't work with a get([]) so this is the best way to handle this.
        //

        // Loop through records and add values to array
        $cloud_account_user_rows = [];
        foreach($cloud_account->cloudAccountUsers as $user) {
            $cloud_account_user_rows[] = [
                'short_id' => $user->short_id,
                'auth_user_short_id' => $user->authUser->short_id,
                'username' => $user->username,
                'full_name' => $user->authUser->full_name,
                'email' => $user->authUser->email,
                'created_at' => $user->created_at->toIso8601String(),
                'provisioned_at' => $user->provisioned_at ? $user->provisioned_at->toIso8601String() : 'not provisioned',
                'state' => $user->state
            ];
        }

        $this->comment('');
        $this->comment('Cloud Account Users');

        // If rows exists, render a table or return a no results found message
        if(count($cloud_account_user_rows) > 0) {
            $this->table(
                ['User Short ID', 'Auth User Short ID', 'Username', 'Full Name', 'Email', 'Created at'],
                $cloud_account_user_rows
            );
        } else {
            $this->error('No cloud account users have been created.');
        }

        //
        // Cloud Account - Child Relationship - Cloud Account User Roles
        // --------------------------------------------------------------------
        // Loop through cloud account user roles in Eloquent model and add
        // calculated values to array. The dot notation for pivot relationships
        // doesn't work with a get([]) so this is the best way to handle this.
        //

        // Loop through records and add values to array
        $cloud_account_user_role_rows = [];
        foreach($cloud_account->cloudAccountUsersRoles()->orderBy('cloud_account_role_id')->get() as $user_role) {
            $cloud_account_user_role_rows[] = [
                'short_id' => $user_role->short_id,
                'cloud_account_user' => '['.$user_role->cloudAccountUser->short_id.'] '.$user_role->cloudAccountUser->username,
                'cloud_account_role' => '['.$user_role->cloudAccountRole->short_id.'] '.$user_role->cloudAccountRole->api_name,
                'created_at' => $user_role->created_at->toIso8601String(),
                'expires_at' => $user_role->expires_at ? $user_role->expires_at->toIso8601String() : 'never'
            ];
        }

        $this->comment('');
        $this->comment('Cloud Account User Roles');

        // If rows exists, render a table or return a no results found message
        if(count($cloud_account_user_role_rows) > 0) {
            $this->table(
                ['User Role Short ID', 'Cloud Account User', 'Cloud Account Role', 'Created at', 'Expires at'],
                $cloud_account_user_role_rows
            );
        } else {
            $this->error('No cloud account roles have been created.');
        }

        $this->comment('');

    }
}
