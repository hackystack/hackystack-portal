<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class CloudRealmDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-realm:delete
                            {short_id? : The short ID of the realm.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete a Cloud Realm by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short_id to lookup the record.');
            $this->line('You can get a list of cloud realms using `cloud-realm:list`');
            $this->line('You can delete by short ID using `cloud-realm:delete a1b2c3d4`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_realm = Models\Cloud\CloudRealm::query()
                ->withCount([
                    'cloudAccounts'
                ])->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_realm == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // If cloud accounts are attached to this realm, abort deletion
        if($cloud_realm->cloud_accounts_count > 0) {
            $this->error('Error: You cannot delete a realm that has cloud accounts attached.');
            $this->line('You can get a list of cloud accounts associated with this realm using `cloud-realm:get '.$cloud_realm->short_id.'`');
            $this->error('');
            die();
        }

        // Cloud Realm Values
        $this->comment('');
        $this->comment('Cloud Realm');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $cloud_realm->id
                ],
                [
                    'short_id',
                    $cloud_realm->short_id
                ],
                [
                    'auth_tenant_id',
                    $cloud_realm->auth_tenant_id
                ],
                [
                    'cloud_billing_account_id',
                    $cloud_realm->cloud_billing_account_id
                ],
                [
                    'cloud_provider_id',
                    $cloud_realm->cloud_provider_id
                ],
                [
                    'name',
                    $cloud_realm->name
                ],
                [
                    'slug',
                    $cloud_realm->slug
                ],
                [
                    'created_at',
                    $cloud_realm->created_at
                ],
            ]
        );

        $this->comment('');

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the deletion of the record?')) {
            $this->error('Error: You aborted. The record still exists.');
            die();
        }

        // Initialize service
        $cloudRealmService = new Services\V1\Cloud\CloudRealmService();

        // Use service to delete record
        $cloudRealmService->delete($cloud_realm->id);

        // Show result in console
        $this->comment('Record deleted successfully.');
        $this->comment('');

    }
}
