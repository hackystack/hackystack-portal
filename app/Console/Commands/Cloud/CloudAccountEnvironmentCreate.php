<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class CloudAccountEnvironmentCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-environment:create
                            {--T|auth_tenant_slug= : The slug of the tenant this environment template belongs to}
                            {--P|cloud_provider_slug= : The slug of the Cloud Provider this Cloud Account belongs to}
                            {--C|cloud_account_slug= : The slug of the Cloud Account this environment belongs to.}
                            {--E|cloud_account_environment_template_name= : The name of the environment template to use for this environment.}
                            {--N|name= : The alpha-dash name of this environment.}
                            {--D|description= : The display description of this environment template. If blank, the GitLab project description from the API will be shown.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a Cloud Account Environment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Cloud Account Environment - Create Record');

        //
        // Auth Tenant
        // --------------------------------------------------------------------
        //

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant should this cloud account belong to?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Cloud Provider
        // --------------------------------------------------------------------
        //

        // Get provider slug
        $cloud_provider_slug = $this->option('cloud_provider_slug');

        // If provider slug option was specified, lookup by slug
        if($cloud_provider_slug) {
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_provider_slug)
                ->first();
        }

        // If provider slug was not provided, prompt for input
        else {

            // Get list of providers to show in console
            $cloud_providers = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            $this->line('');
            $this->line('Available cloud providers: '.implode(',', Arr::flatten($cloud_providers)));

            $cloud_provider_prompt = $this->anticipate('Which cloud provider should this cloud account belong to?', Arr::flatten($cloud_providers));

            // Lookup cloud provider based on slug provided in prompt
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_provider_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$cloud_provider) {
            $this->error('Error: No cloud provider was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Cloud Account Environment Template
        // --------------------------------------------------------------------
        //

        // Get template slug
        $cloud_account_environment_template_name = $this->option('cloud_account_environment_template_name');

        // If template slug option was specified, lookup by slug
        if($cloud_account_environment_template_name) {
            $cloud_account_environment_template = Models\Cloud\CloudAccountEnvironmentTemplate::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->where('git_name', $cloud_account_environment_template_name)
                ->first();
        }

        // If template slug was not provided, prompt for input
        else {

            // Get list of templates to show in console
            $cloud_account_environment_templates = Models\Cloud\CloudAccountEnvironmentTemplate::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->get(['git_name'])
                ->toArray();

            $cloud_account_environment_template_prompt = $this->choice('Which environment template should this environment use?', Arr::flatten($cloud_account_environment_templates));

            // Lookup cloud account based on slug provided in prompt
            $cloud_account_environment_template = Models\Cloud\CloudAccountEnvironmentTemplate::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->where('git_name', $cloud_account_environment_template_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$cloud_account_environment_template) {
            $this->error('Error: No environment template was found with that name.');
            $this->error('');
            die();
        }

        //
        // Cloud Account
        // --------------------------------------------------------------------
        //

        // Get account slug
        $cloud_account_slug = $this->option('cloud_account_slug');

        // If account slug option was specified, lookup by slug
        if($cloud_account_slug) {
            $cloud_account = Models\Cloud\CloudAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->where('slug', $cloud_account_slug)
                ->first();
        }

        // If account slug was not provided, prompt for input
        else {

            // Get list of accounts to show in console
            $cloud_accounts = Models\Cloud\CloudAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->get(['slug'])
                ->toArray();

            $cloud_account_prompt = $this->choice('Which cloud account should this environment belong to?', Arr::flatten($cloud_accounts));

            // Lookup cloud account based on slug provided in prompt
            $cloud_account = Models\Cloud\CloudAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->where('slug', $cloud_account_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$cloud_account) {
            $this->error('Error: No cloud account was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Text Fields
        // --------------------------------------------------------------------
        //

        // Name
        $name = $this->option('name');
        if($name == null) {
            $name = $this->ask('What is the alpha-dash name of this environment?');
        }

        // Description
        $description = $this->option('description');
        if($description == null) {
            $description = $this->ask('What is the display description of this environment?');
        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            // Verify inputs table
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'auth_tenant',
                        '['.$auth_tenant->short_id.'] '.$auth_tenant->slug
                    ],
                    [
                        'cloud_provider',
                        '['.$cloud_provider->short_id.'] '.$cloud_provider->slug
                    ],
                    [
                        'cloud_account_environment_template',
                        '['.$cloud_account_environment_template->short_id.'] '.$cloud_account_environment_template->name
                    ],
                    [
                        'cloud_account',
                        '['.$cloud_account->short_id.'] '.$cloud_account->slug
                    ],
                    [
                        'name',
                        $name
                    ],
                    [
                        'description',
                        $description
                    ],
                ]
            );

            // Ask for confirmation to abort creation.
            if($this->confirm('Do you want to abort the creation of the record?')) {
                $this->error('Error: You aborted. No record was created.');
                die();
            }

        }

        // Initialize service
        $cloudAccountEnvironmentService = new Services\V1\Cloud\CloudAccountEnvironmentService();

        // Use service to create record
        $record = $cloudAccountEnvironmentService->store([
            'auth_tenant_id' => $auth_tenant->id,
            'cloud_provider_id' => $cloud_provider->id,
            'cloud_account_environment_template_id' => $cloud_account_environment_template->id,
            'cloud_account_id' => $cloud_account->id,
            'name' => $name,
            'description' => $description,
        ]);

        // Show result in console
        $this->comment('Record created successfully.');

        // Call the get method to display the tables of values for the record.
        $this->call('cloud-account-environment:get', [
            'short_id' => $record->short_id
        ]);

    }

}
