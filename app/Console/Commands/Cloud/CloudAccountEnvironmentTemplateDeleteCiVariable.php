<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CloudAccountEnvironmentTemplateDeleteCiVariable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-environment-template:delete-ci-variable
                            {short_id? : The short ID of the environment template.}
                            {--key= : The CI variable key (Ex. MY_ENVIRONMENT_VARIABLE)}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete a CI Variable from a Cloud Account Environment Template';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Cloud Account Environment Template - Delete CI Variable');

        // If short ID was specified, lookup by ID
        if($this->argument('short_id')) {
            $cloud_account_environment_template = Models\Cloud\CloudAccountEnvironmentTemplate::query()
                ->where('short_id', $this->argument('short_id'))
                ->first();
        }

        // If short ID or slug is not set, return an error message
        else {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of environment templates using `cloud-account-environment-template:list`');
            $this->line('You can lookup by short ID using `cloud-account-environment-template:create-ci-variable a1b2c3d4`');
            $this->line('');
            die();
        }

        // Check if key option is set
        if($this->option('key')) {
            $key = $this->option('key');
        } else {
            $key = $this->choice('What is the key (name) of the CI variable you want to delete from this template?', array_keys($cloud_account_environment_template->git_ci_variables));
        }

        if(!array_key_exists($key, $cloud_account_environment_template->git_ci_variables)) {
            $this->error('The CI variable key provided does not exist on this template. No changes have occurred.');
            die();
        } else {
            $ci_variable = $cloud_account_environment_template->git_ci_variables[$key];
        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            $this->comment('Cloud Account Environment Template CI Variable');

            // Verify inputs table
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'template',
                        '['.$cloud_account_environment_template->short_id.'] '.$cloud_account_environment_template->rendered_name
                    ],
                    [
                        'key',
                        $ci_variable['key']
                    ],
                    [
                        'dynamic_type',
                        $ci_variable['dynamic_type']
                    ],
                    [
                        'value',
                        $ci_variable['value']
                    ],
                    [
                        'variable_type',
                        $ci_variable['variable_type']
                    ],
                    [
                        'protected',
                        $ci_variable['protected']
                    ],
                    [
                        'masked',
                        $ci_variable['masked']
                    ],
                    [
                        'environment_scope',
                        $ci_variable['environment_scope']
                    ],
                ]
            );

            // Ask for confirmation to abort update.
            if($this->confirm('Do you want to abort the deletion of the CI variable from the template?')) {
                $this->error('Error: You aborted. The template was not updated.');
                die();
            }

        }

        // Define array using existing database values and merge new CI variable into array
        $ci_variables_array = $cloud_account_environment_template->git_ci_variables;
        unset($ci_variables_array[$key]);

        // Update database array
        $cloud_account_environment_template->git_ci_variables = $ci_variables_array;
        $cloud_account_environment_template->save();

        $this->comment('CI variable deleted successfully.');

    }

}
