<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CloudAccountEnvironmentGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-environment:get
                            {short_id? : The short ID of the environment.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a Cloud Account Environment by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of accounts using `cloud-account-environment:list`');
            $this->line('You can lookup by short ID using `cloud-account-environment:get a1b2c3d4`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_account_environment = Models\Cloud\CloudAccountEnvironment::with([
                    'authTenant',
                    'cloudProvider',
                    // 'cloudAccountEnvironments'
                ])->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_account_environment == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Cloud Account Values
        $this->comment('');
        $this->comment('Cloud Account Environment');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $cloud_account_environment->id
                ],
                [
                    'short_id',
                    $cloud_account_environment->short_id
                ],
                [
                    'auth_tenant_id',
                    '['.$cloud_account_environment->authTenant->short_id.'] '.$cloud_account_environment->authTenant->slug
                ],
                [
                    'cloud_provider_id',
                    '['.$cloud_account_environment->cloudProvider->short_id.'] '.$cloud_account_environment->cloudProvider->slug
                ],
                [
                    'cloud_account_environment_template_id',
                    '['.$cloud_account_environment->cloudAccountEnvironmentTemplate->short_id.'] '.$cloud_account_environment->cloudAccountEnvironmentTemplate->rendered_name
                ],
                [
                    'cloud_account_id',
                    '['.$cloud_account_environment->cloudAccount->short_id.'] '.$cloud_account_environment->cloudAccount->slug
                ],
                [
                    'name',
                    $cloud_account_environment->name
                ],
                [
                    'description',
                    $cloud_account_environment->description
                ],
                [
                    'state',
                    $cloud_account_environment->state
                ]
            ]
        );

        if($cloud_account_environment->git_meta_data != null) {

            $git_meta_data_values = [];
            foreach($cloud_account_environment->git_meta_data as $meta_data_key => $meta_data_value) {
                $git_meta_data_values[] = [
                    $meta_data_key,
                    Str::limit(is_array($meta_data_value) ? json_encode($meta_data_value) : $meta_data_value, 100, '...')
                ];
            }

            $this->newLine();
            $this->table(
                ['Git Meta Data Column', 'API Value'],
                $git_meta_data_values
            );
            $this->newLine();

        } else {

            $this->newLine();
            $this->line('<fg=red>Git Meta Data is empty. Check logs for errors with the CloudAccountEnvironment::updateGitMetaData method</>');
            $this->newLine();

        }

        $this->comment('CI Variables');
            $this->table(
                ['key', 'value', 'protected', 'masked', 'environment_scope'],
                $cloud_account_environment->git_ci_variables
            );
            $this->newLine();

        //
        // Child Relationship - Cloud Account Environments
        // --------------------------------------------------------------------
        // Loop through cloud account environments in Eloquent model and add
        // calculated values to array. The dot notation for pivot relationships
        // doesn't work with a get([]) so this is the best way to handle this.
        //
/*
        // Loop through records and add values to array
        $cloud_account_environments_rows = [];
        foreach($cloud_account_environment_templates->cloudAccountEnvironments as $environment) {
            $cloud_account_environments_rows[] = [
                'short_id' => $environment->short_id,
                'cloud_account' => '['.$environment->cloudAccount->short_id.'] '.$environment->cloudAccount->slug,
                'git_project_id' => $environment->git_name,
                'name' => $environment->name,
                'state' => $environment->state,
                'created_at' => $environment->created_at->format('Y-m-d')
            ];
        }

        $this->comment('');
        $this->comment('Cloud Account Environments');

        // If rows exists, render a table or return a no results found message
        if(count($cloud_account_environment_rows) > 0) {
            $this->table(
                ['Environment Short ID', 'Cloud Account', 'Git Project ID', 'Name', 'State', 'Created'],
                $cloud_account_role_rows
            );
        } else {
            $this->error('No cloud account environments have been created.');
        }

        $this->newLine();
*/
    }
}
