<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthUserDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-user:delete
                            {short_id_email? : The short ID or email address of the user.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete an Authentication User by short ID or email address';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id_email = $this->argument('short_id_email');

        // If short ID or slug is not set, return an error message
        if ($short_id_email == null) {
            $this->error('You did not specify the short_id or email to lookup the record.');
            $this->comment('You can lookup by short ID using `auth-user:delete a1b2c3d4 or auth-user:delete dmurphy@example.com`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif ($short_id_email != null) {
            $auth_user = Models\Auth\AuthUser::query()
                ->where('short_id', $short_id_email)
                ->orWhere('email', $short_id_email)
                ->first();
        }

        // If record not found, return an error message
        if ($auth_user == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Call the get method to display the tables of values for the record.
        $this->call('auth-user:get', [
            'short_id_email' => $auth_user->short_id
        ]);

        // Ask for confirmation to abort creation.
        if ($this->confirm('Do you want to abort the deletion of the user and cloud account users?')) {
            $this->error('Error: You aborted. The record still exists.');
            die();
        }

        // Initialize service
        $authUserService = new Services\V1\Auth\AuthUserService();

        // Use service to delete record
        $authUserService->delete($auth_user->id);

        // Show result in console
        $this->comment('Record deleted successfully.');
        $this->comment('');
    }
}
