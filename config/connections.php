<?php

/**
 * ------------------------------------------------------------------------
 * API Connections Configuration
 * ------------------------------------------------------------------------
 *
 * Each service account is considered a "connection" and has an array key
 * that we refer to as the "connection key" that contains a array of
 * configuration values and is used when the ApiClient is instantiated.
 *
 * You can create one or more service accounts with different roles and
 * permissions based on your least privilege model. You can add additional
 * connection keys for each of your service accounts using a snake_case
 * name of your choosing.
 *
 * Security Warning: Do NOT add any sensitive values in this file. You must
 * define all values in your `.env` file or server's environment variables.
 */

return [

    /**
     * ------------------------------------------------------------------------
     *  Default Connections for each provider
     * ------------------------------------------------------------------------
     */

    'default' => [
        'gitlab' => env('CONNECTIONS_DEFAULT_GITLAB', 'saas'),
        'google' => env('CONNECTIONS_DEFAULT_GOOGLE', 'cloud'),
        'okta' => env('CONNECTIONS_DEFAULT_OKTA', 'prod')
    ],

    /**
     * ------------------------------------------------------------------------
     * Google Service Accounts
     * ------------------------------------------------------------------------
     *
     * @param array $api_scopes
     *      The API OAUTH scopes that will be needed for the Google Workspace
     *      API endpoints that will be used. These need to match what you have
     *      granted your service account.
     *      https://developers.google.com/identity/protocols/oauth2/scopes
     *
     *      ```php
     *      [
     *          'https://www.googleapis.com/auth/admin.directory.group',
     *          'https://www.googleapis.com/auth/admin.directory.user'
     *      ]
     *      ```
     *
     * @param string $json_key
     *      If you are using environment variables, you can set the JSON key
     *      contents as a string. Do NOT copy this string to this file.
     *
     * @param string $json_key_file_path
     *      You can specify the full operating system path to the JSON key file.
     *
     *      If null, the GCP service account JSON API key file that you generate
     *      and download should be added to your desired secure directory or the
     *      local `storage/keys/glamstack-google-workspace` directory.
     *
     *      ```php
     *      storage('keys/google-workspace-service-account.json')
     *      ```
     *
     *      ```php
     *      /etc/nginx/keys/google-workspace-service-account.json
     *      ```
     *
     * @param string $customer
     *      This is an alias for $customer_id due to API key variances. There is
     *      no need to set a different environment variable for this value.
     *
     * @param string $customer_id
     *      The customer number of the Google Account that the API's will be run
     *      on. This will need to match the customer number that the Service
     *      Account is under as well or it will not work.
     *
     * @param string $domain
     *      The domain in the Google Organization to filter results to. This
     *      should match the domain that the Service Account is created under.
     *
     * @param ?string $subject_email
     *      The email of the address to run the Google Workspace API as. If this
     *      is not set then it will use the client_email from the JSON Key.
     */

    'google' => [
        'workspace' => [
            'api_scopes' => [
                'https://www.googleapis.com/auth/admin.directory.group',
                'https://www.googleapis.com/auth/admin.directory.user'
            ],
            'json_key' => env('CONNECTIONS_GOOGLE_WORKSPACE_JSON_KEY'),
            'json_key_file_path' => env('CONNECTIONS_GOOGLE_WORKSPACE_JSON_KEY_FILE_PATH'),
            'customer' => env('CONNECTIONS_GOOGLE_WORKSPACE_CUSTOMER_ID'),
            'customer_id' => env('CONNECTIONS_GOOGLE_WORKSPACE_CUSTOMER_ID'),
            'domain' => env('CONNECTIONS_GOOGLE_WORKSPACE_DOMAIN'),
            'subject_email' => env('CONNECTIONS_GOOGLE_WORKSPACE_SUBJECT_EMAIL')
        ],
        'cloud' => [
            'api_scopes' => [
                'https://www.googleapis.com/auth/cloud-platform'
            ],
            'json_key' => env('CONNECTIONS_GOOGLE_CLOUD_JSON_KEY'),
            'json_key_file_path' => env('CONNECTIONS_GOOGLE_CLOUD_JSON_KEY_FILE_PATH'),
            'customer' => env('CONNECTIONS_GOOGLE_CLOUD_CUSTOMER_ID'),
            'customer_id' => env('CONNECTIONS_GOOGLE_CLOUD_CUSTOMER_ID'),
            'domain' => env('CONNECTIONS_GOOGLE_CLOUD_DOMAIN'),
            'subject_email' => env('CONNECTIONS_GOOGLE_CLOUD_SUBJECT_EMAIL')
        ],
    ],
];
