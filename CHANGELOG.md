# HackyStack Portal Changelog

You can find the changelog file for each release in the [changelog/](changelog/) directory.

## Disclaimer

* The Changelog is programatically generated using the [developer:generate-changelog](https://gitlab.com/hackystack/hackystack-portal/-/blob/main/app/Console/Commands/Developer/GenerateChangelog.php) command.
* The nomencalture standards, syntax and spelling of merge request and commit descriptions will vary and are considered best effort by the project contributors and maintainers.
* Any erroneous work-in-progress (WIP) commit messages are not removed prior to adding release notes to the Changelog.
* The `changelog-type::___` scoped label is applied to merge requests to automatically categorize merge requests and related commits. Some merge requests may apply to multiple categories, however we apply the category based on the predominant category of commits in the merge request.
* The `New Features` and `Limitations` sections for each release are written in the milestone description by the project maintainers and are designed to provide a high-level overview of the release notes. For technical details, you are encouraged to review the descriptions of the merge requests and commits.
* The `Merge Requests` and `Commits` sections for each release are sorted alphabetically by category and then by description. Note that capitalized letters and lowercase letters are sorted as separate ranges due to the Laravel collections sort functionality.
